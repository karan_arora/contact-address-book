# Project Title

Address Book Backend
This project has 3 address books by default with 3 contacts each. The user can add, delete, edit contact details. The user can also create new address books or delete existing ones


## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/address-book-group/contact-address-book.git
```

Go to the project directory

```bash
  cd contact-address-book
```

Import project in an editor

```bash
  Import the project in any java editor (preferably intellij) and run the project (This project is created using Java 11 so please ensure compatibility). The API's should be accessible via localhost:8080
```

