package com.assignment.addressbook.controller;

import com.assignment.addressbook.model.AddressBookModel;
import com.assignment.addressbook.model.ContactDetailsModel;
import com.assignment.addressbook.model.ContactIdModel;
import com.assignment.addressbook.model.ServiceResponseWrapperModel;
import com.assignment.addressbook.service.AddressBookService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import static org.junit.jupiter.api.Assertions.assertEquals;


@ExtendWith(MockitoExtension.class)
class AddressBookControllerTest {

    @InjectMocks
    private AddressBookController addressBookController;

    @Mock
    private AddressBookService addressBookService;

    @Test
    void fetchAllAddressBooks() {
        ServiceResponseWrapperModel<Object> serviceResponseWrapperModel = new ServiceResponseWrapperModel<>(addressBookService.fetchAddressBookNames());
        ResponseEntity responseEntity = new ResponseEntity(serviceResponseWrapperModel, HttpStatus.OK);
        assertEquals(responseEntity,addressBookController.fetchAllAddressBooks());
    }

    @Test
    void createAddressBook() {
        AddressBookModel addressBookModel = new AddressBookModel("ADDRESS_BOOK_1");
        ServiceResponseWrapperModel<Object> serviceResponseWrapperModel = new ServiceResponseWrapperModel<>(addressBookService.createAddressBook(addressBookModel));
        ResponseEntity<Object> responseEntity = new ResponseEntity<>(serviceResponseWrapperModel, HttpStatus.OK);
        assertEquals(responseEntity,addressBookController.createAddressBook(addressBookModel));
    }

    @Test
    void deleteAddressBook() {
        ServiceResponseWrapperModel<Object> serviceResponseWrapperModel = new ServiceResponseWrapperModel<>(addressBookService.deleteAddressBook("ADDRESS_BOOK_1"));
        ResponseEntity responseEntity = new ResponseEntity(serviceResponseWrapperModel, HttpStatus.OK);
        String addressBookModel = "ADDRESS_BOOK_1";
        assertEquals(responseEntity,addressBookController.deleteAddressBook(addressBookModel));
    }

    @Test
    void createContact() {
        ContactDetailsModel model = new ContactDetailsModel(1,"John","9463829204","ADDRESS_BOOK_1");
        ServiceResponseWrapperModel<Object> serviceResponseWrapperModel = new ServiceResponseWrapperModel<>(addressBookService.createContact(model));
        ResponseEntity responseEntity = new ResponseEntity(serviceResponseWrapperModel, HttpStatus.OK);
        assertEquals(responseEntity,addressBookController.createContact(model));
    }

    @Test
    void fetchContact() {
        ContactIdModel id = new ContactIdModel(1);
        ServiceResponseWrapperModel<Object> serviceResponseWrapperModel = new ServiceResponseWrapperModel<>(addressBookService.fetchContact(id));
        ResponseEntity responseEntity = new ResponseEntity(serviceResponseWrapperModel, HttpStatus.OK);
        assertEquals(responseEntity,addressBookController.fetchContact(id));
    }

    @Test
    void updateContact() {
        ContactDetailsModel model = new ContactDetailsModel(1,"John","9463829204","ADDRESS_BOOK_1");
        ServiceResponseWrapperModel<Object> serviceResponseWrapperModel = new ServiceResponseWrapperModel<>(addressBookService.updateContact(model));
        ResponseEntity responseEntity = new ResponseEntity(serviceResponseWrapperModel, HttpStatus.OK);
        assertEquals(responseEntity,addressBookController.updateContact(model));
    }

    @Test
    void deleteContact() {
        Integer id = 1;
        ServiceResponseWrapperModel<Object> serviceResponseWrapperModel = new ServiceResponseWrapperModel<>(addressBookService.deleteContact(id));
        ResponseEntity responseEntity = new ResponseEntity(serviceResponseWrapperModel, HttpStatus.OK);
        assertEquals(responseEntity,addressBookController.deleteContact(id));
    }

    @Test
    void fetchContactsFromAddressBook() {
        AddressBookModel addressBookModel = new AddressBookModel("ADDRESS_BOOK_1");
        ServiceResponseWrapperModel<Object> serviceResponseWrapperModel = new ServiceResponseWrapperModel<>(addressBookService.fetchContactsFromAddressBook(addressBookModel));
        ResponseEntity responseEntity = new ResponseEntity(serviceResponseWrapperModel, HttpStatus.OK);
        assertEquals(responseEntity,addressBookController.fetchContactsFromAddressBook(addressBookModel));
    }

    @Test
    void fetchUniqueContacts() {
        ServiceResponseWrapperModel<Object> serviceResponseWrapperModel = new ServiceResponseWrapperModel<>(addressBookService.fetchUniqueContacts());
        ResponseEntity responseEntity = new ResponseEntity(serviceResponseWrapperModel, HttpStatus.OK);
        assertEquals(responseEntity,addressBookController.fetchUniqueContacts());

    }
}