package com.assignment.addressbook.service;

import com.assignment.addressbook.entity.AddressBookEntity;
import com.assignment.addressbook.entity.ContactDetailsEntity;
import com.assignment.addressbook.model.AddressBookModel;
import com.assignment.addressbook.model.ContactDetailsModel;
import com.assignment.addressbook.model.ContactIdModel;
import com.assignment.addressbook.model.UniqueContactsModel;
import java.util.List;

public interface AddressBookService {
    public List<String> fetchAddressBookNames();

    public AddressBookEntity createAddressBook(AddressBookModel addressBookName);

    public String deleteAddressBook(String addressBookName);

    public ContactDetailsEntity createContact(ContactDetailsModel contactDetails);

    public ContactDetailsEntity fetchContact(ContactIdModel id);

    public ContactDetailsEntity updateContact(ContactDetailsModel contactDetails);

    public String deleteContact(Integer id);

    public List<ContactDetailsEntity> fetchContactsFromAddressBook(AddressBookModel addressBookModel);

    public List<UniqueContactsModel> fetchUniqueContacts();

}
