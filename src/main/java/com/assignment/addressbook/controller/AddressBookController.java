package com.assignment.addressbook.controller;

import com.assignment.addressbook.entity.AddressBookEntity;
import com.assignment.addressbook.entity.ContactDetailsEntity;
import com.assignment.addressbook.model.UniqueContactsModel;
import com.assignment.addressbook.model.AddressBookModel;
import com.assignment.addressbook.model.ContactDetailsModel;
import com.assignment.addressbook.model.ContactIdModel;
import com.assignment.addressbook.model.ServiceResponseWrapperModel;
import com.assignment.addressbook.service.AddressBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/contact-address-book/v1")
public class AddressBookController {

    @Autowired
    private AddressBookService addressBookService;

    @GetMapping(value="/fetch-all-address-books")
    public ResponseEntity<ServiceResponseWrapperModel> fetchAllAddressBooks(){
        List<String> listAddress = this.addressBookService.fetchAddressBookNames();
        return new ResponseEntity<>(new ServiceResponseWrapperModel(listAddress),HttpStatus.OK);
    }

    @PostMapping(value="/create-address-book")
    public ResponseEntity<ServiceResponseWrapperModel> createAddressBook(@RequestBody AddressBookModel addressBookModel){
        AddressBookEntity addressBook = this.addressBookService.createAddressBook(addressBookModel);
        return new ResponseEntity<>(new ServiceResponseWrapperModel(addressBook),HttpStatus.OK);
    }


    @DeleteMapping(value="/delete-address-book/{addressBookName}")
    public ResponseEntity<ServiceResponseWrapperModel> deleteAddressBook(@PathVariable String addressBookName){
        String response = this.addressBookService.deleteAddressBook(addressBookName);
        return new ResponseEntity<>(new ServiceResponseWrapperModel(response),HttpStatus.OK);
    }

    @PostMapping(value="/create-contact")
    public ResponseEntity<ServiceResponseWrapperModel> createContact(@RequestBody ContactDetailsModel contactDetailsModel){
        ContactDetailsEntity contact = this.addressBookService.createContact(contactDetailsModel);
        return new ResponseEntity<>(new ServiceResponseWrapperModel(contact),HttpStatus.OK);
    }

    @PostMapping(value="/fetch-contact")
    public ResponseEntity<ServiceResponseWrapperModel> fetchContact(@RequestBody ContactIdModel contactIdModel){
        ContactDetailsEntity contactDetails = this.addressBookService.fetchContact(contactIdModel);
        return new ResponseEntity<>(new ServiceResponseWrapperModel(contactDetails),HttpStatus.OK);
    }

    @PostMapping(value="/update-contact")
    public ResponseEntity<ServiceResponseWrapperModel> updateContact(@RequestBody ContactDetailsModel contactDetailsModel){
        ContactDetailsEntity contactDetails = this.addressBookService.updateContact(contactDetailsModel);
        return new ResponseEntity<>(new ServiceResponseWrapperModel(contactDetails),HttpStatus.OK);
    }

    @DeleteMapping(value="/delete-contact/{id}")
    public ResponseEntity<ServiceResponseWrapperModel> deleteContact(@PathVariable Integer id){
        String response = this.addressBookService.deleteContact(id);
        return new ResponseEntity<>(new ServiceResponseWrapperModel(response),HttpStatus.OK);
    }

    @PostMapping(value="/fetch-contacts-from-address-book")
    public ResponseEntity<ServiceResponseWrapperModel> fetchContactsFromAddressBook(@RequestBody AddressBookModel addressBookModel){
        List<ContactDetailsEntity> contacts = this.addressBookService.fetchContactsFromAddressBook(addressBookModel);
        return new ResponseEntity<>(new ServiceResponseWrapperModel(contacts),HttpStatus.OK);
    }

    @GetMapping(value="/fetch-unique-contacts")
    public ResponseEntity<ServiceResponseWrapperModel> fetchUniqueContacts(){
        List<UniqueContactsModel> uniqueContacts = this.addressBookService.fetchUniqueContacts();
        return new ResponseEntity<>(new ServiceResponseWrapperModel(uniqueContacts),HttpStatus.OK);
    }

}
