package com.assignment.addressbook.dao;

import com.assignment.addressbook.entity.ContactDetailsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface ContactDetailsDao extends JpaRepository<ContactDetailsEntity, Integer> {

    Optional<ContactDetailsEntity> findById(Integer id);

    List<ContactDetailsEntity> findByAddressBookName(String addressBook);

    @Modifying
    @Transactional
    @Query("DELETE FROM contactDetails u WHERE u.addressBookName=:addressBook")
    void deleteByAddressBookName(String addressBook);

}
