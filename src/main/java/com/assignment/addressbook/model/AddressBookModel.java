package com.assignment.addressbook.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AddressBookModel {
    @JsonProperty("addressBookName")
    private String addressBookName;

    public String getAddressBookName() {
        return addressBookName;
    }

    public void setAddressBookName(String addressBookName) {
        this.addressBookName = addressBookName;
    }

    public AddressBookModel() {
    }

    public AddressBookModel(String addressBookName) {
        this.addressBookName = addressBookName;
    }
}
