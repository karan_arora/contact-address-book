package com.assignment.addressbook.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ServiceResponseWrapperModel<T> {
    @JsonProperty("data")
    private T data;

    @JsonProperty("errors")
    private CustomErrorModel errors;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public CustomErrorModel getErrors() {
        return errors;
    }

    public void setErrors(CustomErrorModel errors) {
        this.errors = errors;
    }

    public ServiceResponseWrapperModel() {
    }

    public ServiceResponseWrapperModel(T data) {
        this.data = data;
    }

    public ServiceResponseWrapperModel(T data, CustomErrorModel errors) {
        this.data = data;
        this.errors = errors;
    }
}
