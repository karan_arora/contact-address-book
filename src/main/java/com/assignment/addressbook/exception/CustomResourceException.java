package com.assignment.addressbook.exception;

public class CustomResourceException extends RuntimeException{
    private String errMsg;

    public CustomResourceException(String errMsg){
        super(errMsg);
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }
}
